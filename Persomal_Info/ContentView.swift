//
//  ContentView.swift
//  Persomal_Info
//
//  Created by Andrickson Coste on 8/19/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
            Color(red: 0.09, green: 0.63, blue: 0.52)
                .edgesIgnoringSafeArea(.all)
            VStack {
                Image("ready")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 150.0, height: 150.0)
                    .clipShape(Circle())
                    .overlay(
                        Circle().stroke(Color.white, lineWidth: 5)
                    )
                
                Text("Andrickson Coste")
                    .font(Font.custom("EBGaramond-Italic", size: 30))
                    .bold()
                    .foregroundColor(.white)
                    .padding()
                Text("iOS Developer")
                    .foregroundColor(.white)
                    .font(.system(size: 25))
                Divider()
                infoView(text: "+1 (800)-564-9878", imageName: "phone.fill", fillColor: .white)
                infoView(text: "brrr23@maildrop.cc", imageName: "envelope.fill", fillColor: .white)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct infoView: View {
    
    let text: String
    let imageName: String
    let fillColor: Color
    
    var body: some View {
        RoundedRectangle(cornerRadius: 25)
            .fill(fillColor)
            .frame(width: 350, height: 50)
            .overlay(HStack {
                Image(systemName: imageName)
                    .foregroundColor(.green)
                Text(text)
            })
            .padding(.all)
    }
}
